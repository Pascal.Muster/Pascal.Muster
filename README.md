### Hi there 👋

I am Pascal from Freiberg in Saxony, Germany. I have studied Nanotechnology and am now a PhD Student at RWTH Aachen University and Infineon Technologies in the topic of silicon based scaleable quantum technologies. I am interested in data science, software engineering, microservices, and spin qubits for sure 😅!

- 🔭 I’m currently working on a *waferplot library* project named **PyWaferMaps**.
- 🔭 I'm also refactoring a multiplexer python driver package for JARA IQI named **Muximillian**.
- 🌱 I'm learning *CI / CD within github/gitlab*.
- 🌱 I'm learning linux *server hosting* and have my own server with some usefull microservices running.
- 💬 Ask me about *python, matlab, data analysis* and also *linux server, truenas, truecharts, kubernetes, docker*.
- 📫 How to reach me: [LinkedIn](https://www.linkedin.com/in/muster-pascal/)
- ⚡ Fun fact: It takes a photon up to 40,000 years to travel from the core of the sun to its surface, but only 8 minutes to travel the rest of the way to Earth ...
